set linesize 200
SPOOL log/install.log REPLACE
prompt ******************** Installing DEMO Application  ********************

prompt ******************** Installing tables ******************** 
@@scripts/install_tables.sql
prompt ******************** Installing packages ********************
@@scripts/install_packages.sql
prompt ******************** Installing data ********************
@@scripts/create_data.sql
prompt ******************** Installation complete ********************
SPOOL off