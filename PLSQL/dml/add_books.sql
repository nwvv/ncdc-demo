BEGIN
DOCUMENTS_UTIL.insert_document(1,'Official document 1');
DOCUMENTS_UTIL.insert_document(2,'Formal letter','A formal letter to the County Clerk Office');
DOCUMENTS_UTIL.insert_document(3,'Acceptance letter','Letter of acceptance to the university');
DOCUMENTS_UTIL.insert_document(4,'JEE Spec');
DOCUMENTS_UTIL.insert_document(5,'Black Company','An excerpt form a book by Glen Cook');
COMMIT;
END;
/
