--------------------------------------------------------
--  DDL for Package IMPORT_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "IMPORT_UTIL" AS

  /*
   *  Imports document data from the specified file and directory.
   *  **Important** - the script invoker must have read permission 
   *  for the directory.
   */
  PROCEDURE import_documents(
    p_directory VARCHAR2,
    p_file_name VARCHAR2);

END IMPORT_UTIL;

/
