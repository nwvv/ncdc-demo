--------------------------------------------------------
--  DDL for Package Body DOCUMENTS_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "DOCUMENTS_UTIL" 
AS

  ---------------------------------------------------------------------------
  --                  INTERNAL TYPES                                       --
  ---------------------------------------------------------------------------
  TYPE TAB_DOCUMENT_NAMES IS TABLE OF DOCUMENTS.DOCUMENT_NAME%TYPE;
  ---------------------------------------------------------------------------
  --                  INTERNAL CONSTANTS                                   --
  ---------------------------------------------------------------------------
  C_NO_DESCRIPTION_LABEL CONSTANT VARCHAR2(14) := 'No Description';
  C_NAMES_FETCH_SIZE CONSTANT pls_integer := 500;
  ---------------------------------------------------------------------------
  --                  PRIVATE  FUNCTION DECLARATIONS                       --
  ---------------------------------------------------------------------------
  PROCEDURE print_names(
    p_names TAB_DOCUMENT_NAMES);
  --***********************************************************************--
  ---------------------------------------------------------------------------
  --------                     IMPLEMENTATION                       ---------
  ---------------------------------------------------------------------------
  --***********************************************************************--
  ---------------------------------------------------------------------------
  --                           PUBLIC API IMPLEMENTATION                   --
  ---------------------------------------------------------------------------
  PROCEDURE insert_document(
      p_id DOCUMENTS.DOCUMENT_ID%TYPE,
      p_name DOCUMENTS.DOCUMENT_NAME%TYPE,
      p_description DOCUMENTS.DOCUMENT_DESCRIPTION%TYPE DEFAULT NULL) AS
  BEGIN
    INSERT
  INTO DOCUMENTS
    (
      DOCUMENT_ID,
      DOCUMENT_NAME,
      DOCUMENT_DESCRIPTION
    )
    VALUES
    (
      p_id,
      p_name,
      p_description
    );
  END insert_document;
  -------------------------------------------------
  -------------------------------------------------
  PROCEDURE print_documents AS
    -- Task specification doesn't mention ordering of the output so I made no assumptions about it.
    CURSOR c_names IS
      SELECT
        CASE
          WHEN d.DOCUMENT_DESCRIPTION IS NOT NULL
            THEN d.DOCUMENT_NAME
          ELSE
            NULL
        END
      FROM DOCUMENTS d;

    l_names TAB_DOCUMENT_NAMES;
  BEGIN
    OPEN c_names;
    LOOP
      FETCH c_names BULK COLLECT INTO l_names LIMIT C_NAMES_FETCH_SIZE;
      print_names(l_names);
      EXIT WHEN c_names%NOTFOUND;
    END LOOP;
    CLOSE c_names;
  EXCEPTION WHEN OTHERS THEN
    IF c_names%ISOPEN THEN
      CLOSE c_names;
    END IF;
    RAISE;
  END print_documents;
  -------------------------------------------------
  -------------------------------------------------
  FUNCTION validate_pesel(
     p_pesel NUMBER)
    RETURN BOOLEAN IS
  BEGIN
	--The delegate method has been left here to conform to requirements. All of the implementation
	--has been placed in a separate package dedicated to pesel validation.
    RETURN PESEL_UTIL.validate_pesel(p_pesel);
  END validate_pesel;

  ---------------------------------------------------------------------------
  --                      PRIVATE FUNCTION IMPLEMENTATION                  --
  ---------------------------------------------------------------------------

  PROCEDURE print_names(p_names TAB_DOCUMENT_NAMES)
  IS
  BEGIN
    /* Currently, we can safely use regular "for loop" as this is a package-private
     * procedure and we inted for the names collection passed in
     * to be densely filled. Should this procedure
     * be publicly exposed we should consider using "while loop" to
     * iterate over names to allow passing in sparsely populated collections.
     */
    FOR i in 1..p_names.COUNT
    LOOP
      DBMS_OUTPUT.PUT_LINE(COALESCE(p_names(i),C_NO_DESCRIPTION_LABEL));
    END LOOP;
  END print_names;
  -------------------------------------------------
  -------------------------------------------------


END DOCUMENTS_UTIL;

/
