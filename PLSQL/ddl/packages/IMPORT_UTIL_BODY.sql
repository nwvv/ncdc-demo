--------------------------------------------------------
--  DDL for Package Body IMPORT_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "IMPORT_UTIL" AS

  ---------------------------------------------------------------------------
  --                  INTERNAL TYPES                                       --
  ---------------------------------------------------------------------------

  ---------------------------------------------------------------------------
  --                  INTERNAL CONSTANTS                                   --
  ---------------------------------------------------------------------------
  C_EXPECTED_CHILDREN_COUNT PLS_INTEGER := 3;
  C_IMPORT_ERROR_CODE CONSTANT PLS_INTEGER := -20001;
  C_FIELDS_NAME CONSTANT VARCHAR2(4CHAR) := 'name';
  C_FIELDS_DESCRIPTION CONSTANT VARCHAR2(11CHAR) := 'description';
  C_FIELDS_ID CONSTANT VARCHAR2(2CHAR) := 'id';
  ---------------------------------------------------------------------------
  --                  PRIVATE  FUNCTION DECLARATIONS                       --
  ---------------------------------------------------------------------------
  PROCEDURE raise_format_error;
  PROCEDURE raise_import_error(p_message VARCHAR2);
  FUNCTION get_text_from_node(
    p_node xmldom.DOMNode)
  RETURN VARCHAR2;
  PROCEDURE get_child_text(
    p_document xmldom.DOMElement,
    p_element_name VARCHAR2,
    p_value OUT NOCOPY VARCHAR2);
  PROCEDURE validate_child_count(
    p_document xmldom.DOMElement);
  PROCEDURE add_document(
      p_document xmldom.DOMElement);
  PROCEDURE add_documents(
    p_document xmldom.DOMDocument);
  FUNCTION set_up_parser(
    p_directory VARCHAR2)
  RETURN xmlparser.parser;
  --***********************************************************************--
  ---------------------------------------------------------------------------
  --------                     IMPLEMENTATION                       ---------
  ---------------------------------------------------------------------------
  --***********************************************************************--
  ---------------------------------------------------------------------------
  --                           PUBLIC API IMPLEMENTATION                   --
  ---------------------------------------------------------------------------

  PROCEDURE import_documents(
    p_directory VARCHAR2,
    p_file_name VARCHAR2)
  AS
    l_parser xmlparser.parser;
    doc xmldom.DOMDocument;
  BEGIN
       l_parser := set_up_parser(p_directory);
       xmlparser.parse(l_parser, p_directory || '/' || p_file_name);
       doc := xmlparser.getDocument(l_parser);
       add_documents(doc);
    exception

    when xmldom.INDEX_SIZE_ERR then
       raise_import_error('Index Size error');

    when xmldom.DOMSTRING_SIZE_ERR then
       raise_import_error('DOM String Size error');

    when xmldom.HIERARCHY_REQUEST_ERR then
       raise_import_error('Hierarchy request error');

    when xmldom.WRONG_DOCUMENT_ERR then
       raise_import_error('Wrong document error');

    when xmldom.INVALID_CHARACTER_ERR then
       raise_import_error('Invalid Character error');

    when xmldom.NO_DATA_ALLOWED_ERR then
       raise_import_error('No data allowed error');

    when xmldom.NO_MODIFICATION_ALLOWED_ERR then
       raise_import_error('No modification allowed error');

    when xmldom.NOT_FOUND_ERR then
       raise_import_error('Not found error');

    when xmldom.NOT_SUPPORTED_ERR then
       raise_import_error('Not supported error');

  END import_documents;

  ---------------------------------------------------------------------------
  --                      PRIVATE FUNCTION IMPLEMENTATION                  --
  ---------------------------------------------------------------------------
  PROCEDURE raise_import_error(p_message VARCHAR2)
  IS
  BEGIN
    raise_application_error(C_IMPORT_ERROR_CODE, p_message);
  END raise_import_error;

  PROCEDURE raise_format_error
  IS
  BEGIN
    raise_import_error('Incorrect input format');
  END raise_format_error;

  -------------------------------------------------
  -------------------------------------------------
  FUNCTION get_text_from_node(
    p_node xmldom.DOMNode)
  RETURN VARCHAR2
  IS
    l_text_node xmldom.DOMNode;
  BEGIN
    l_text_node := xmldom.getFirstChild(p_node);
    IF xmldom.isNull(l_text_node) THEN
      RETURN NULL;
    END IF;
    IF xmldom.getNodeType(l_text_node) = xmldom.TEXT_NODE THEN
      RETURN xmldom.getNodeValue(l_text_node);
    ELSE
      raise_format_error;
    END IF;
  END get_text_from_node;
  -------------------------------------------------
  -------------------------------------------------
  PROCEDURE get_child_text(
    p_document xmldom.DOMElement,
    p_element_name VARCHAR2,
    p_value OUT NOCOPY VARCHAR2)
  IS
    l_children xmldom.DOMNodeList;
    l_size NUMBER;
  BEGIN
    l_children := xmldom.getChildrenByTagName(p_document,p_element_name);
    l_size := xmldom.getLength(l_children);
    IF l_size <> 1 THEN
        raise_format_error;
    END IF;
    p_value := get_text_from_node(xmldom.item(l_children,0));
  END get_child_text;
  -------------------------------------------------
  -------------------------------------------------
  PROCEDURE validate_child_count(
    p_document xmldom.DOMElement)
  IS
    l_document_details xmldom.DOMNodeList;
    l_size NUMBER;
  BEGIN
    l_document_details := xmldom.getChildrenByTagName(p_document,'*');
    l_size := xmldom.getLength(l_document_details);
    IF l_size <> C_EXPECTED_CHILDREN_COUNT THEN
        raise_format_error;
    END IF;
  END validate_child_count;
  -------------------------------------------------
  -------------------------------------------------

  PROCEDURE add_document(
      p_document xmldom.DOMElement)
  IS
    l_name VARCHAR2(100);
    l_desc VARCHAR2(100);
    l_id VARCHAR2(100);
  BEGIN
    validate_child_count(p_document);
    get_child_text(p_document,C_FIELDS_NAME,l_name);
    get_child_text(p_document,C_FIELDS_DESCRIPTION,l_desc);
    get_child_text(p_document,C_FIELDS_ID,l_id);
    DOCUMENTS_UTIL.insert_document(p_id=>l_id,p_name=>l_name,p_description=>l_desc);
  END add_document;
  -------------------------------------------------
  -------------------------------------------------

  PROCEDURE add_documents(
    p_document xmldom.DOMDocument)
  IS
    l_documents xmldom.DOMNodeList;
    l_document xmldom.DOMNode;
    l_element xmldom.DOMElement;
    l_documents_count NUMBER;
  BEGIN
    l_documents := xmldom.getElementsByTagName(p_document,'document');
    l_documents_count := xmldom.getLength(l_documents);
    FOR i in 0..l_documents_count-1
    LOOP
      l_document := xmldom.item(l_documents,i);
      l_element := xmldom.makeElement(l_document);
      add_document(l_element);
    END LOOP;
  END add_documents;
  -------------------------------------------------
  -------------------------------------------------

  FUNCTION set_up_parser(
    p_directory VARCHAR2)
  RETURN xmlparser.parser
  IS
    l_parser xmlparser.parser;
  BEGIN
    l_parser := xmlparser.newParser;
    xmlparser.setValidationMode(l_parser, FALSE);
    xmlparser.setBaseDir(l_parser, p_directory);
    RETURN l_parser;
  END set_up_parser;

  -------------------------------------------------
  -------------------------------------------------

END IMPORT_UTIL;

/
