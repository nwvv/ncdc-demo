--------------------------------------------------------
--  DDL for Package PESEL_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PESEL_UTIL" AS
  /*
  * PESEL number utility package, provides methods for PESEL validation.
  * 
  *	SIDE NOTE: Even though this is a small project and it might be 
  * a little bit of overkill in this situation I have decided to 
  * clearly separate all PESEL-related functionality in its own package.
  * From my experience even in case of small projects this kind of an approach
  * pays off, because it makes things easier to find later on and also discourages
  * other contributors from committing poor quality code (the "first broken window" effect).
  * @author Przemyslaw Walat
  */ 
  
  
  /*
  * Validates PESEL numbers. Checks the sign and length. Verifies control number of 
  * the number passed in according to the algorithm described on
  * https://pl.wikipedia.org/wiki/PESEL, "Metoda rownowazna" section.
  * Null safe.  
  *
  * @author Przemyslaw Walat
  */
  FUNCTION validate_pesel(
      p_pesel NUMBER)
    RETURN BOOLEAN;
END PESEL_UTIL;

/
