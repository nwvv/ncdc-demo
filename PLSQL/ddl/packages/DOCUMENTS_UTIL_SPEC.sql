--------------------------------------------------------
--  DDL for Package DOCUMENTS_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "DOCUMENTS_UTIL" 
AS
  /*
  * Documents utility package, provides methods for inserting and listing documents.
  * Also provides PESEL number validation.
  * @author Przemyslaw Walat
  */

  /*
  * Insert a new row into DOCUMENTS
  * @author Przemyslaw Walat
  */
  PROCEDURE insert_document(
      p_id DOCUMENTS.DOCUMENT_ID%TYPE,
      p_name DOCUMENTS.DOCUMENT_NAME%TYPE,
      p_description DOCUMENTS.DOCUMENT_DESCRIPTION%TYPE DEFAULT NULL);
  /*
  * Lists all stored documents.
  * For each record in DOCUMENTS table, if the description is
  * not empty, the document's name is printed to the screen, otherwise
  * prints �No Description� for the document.
  * @author Przemyslaw Walat
  */
  PROCEDURE print_documents;

  /*
  * Validates PESEL numbers. Checks the sign, lenght.Verifies control number of
  * the number passed in according to the algorithm described on
  * https://pl.wikipedia.org/wiki/PESEL, "Metoda rownowazna" section.
  * Null safe.
  *
  * @author Przemyslaw Walat
  */
  FUNCTION validate_pesel(
      p_pesel NUMBER)
    RETURN BOOLEAN;
END DOCUMENTS_UTIL;

/
