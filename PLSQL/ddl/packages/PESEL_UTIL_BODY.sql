--------------------------------------------------------
--  DDL for Package Body PESEL_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PESEL_UTIL" 
AS  

  ---------------------------------------------------------------------------
  --                  PRIVATE  FUNCTION DECLARATIONS                       --
  ---------------------------------------------------------------------------

  FUNCTION validate_pesel_basic(
    p_pesel NUMBER)
  RETURN BOOLEAN;
  FUNCTION validate_control_digit(
    p_pesel NUMBER)
  RETURN BOOLEAN;
  FUNCTION get_digit(
    p_number VARCHAR2,
    p_position PLS_INTEGER)
  RETURN PLS_INTEGER;
  --***********************************************************************--
  ---------------------------------------------------------------------------
  --------                     IMPLEMENTATION                       ---------
  ---------------------------------------------------------------------------
  --***********************************************************************--
  ---------------------------------------------------------------------------
  --                           PUBLIC API IMPLEMENTATION                   --
  ---------------------------------------------------------------------------
  FUNCTION validate_pesel(
     p_pesel NUMBER)
    RETURN BOOLEAN IS
  l_basic_valid BOOLEAN;
  BEGIN
    l_basic_valid := validate_pesel_basic(p_pesel);
    IF l_basic_valid THEN
      RETURN validate_control_digit(p_pesel);
    END IF;
    RETURN FALSE;
  END validate_pesel;

  ---------------------------------------------------------------------------
  --                      PRIVATE FUNCTION IMPLEMENTATION                  --
  ---------------------------------------------------------------------------

  FUNCTION validate_pesel_basic(
     p_pesel NUMBER)
    RETURN BOOLEAN IS
    l_length PLS_INTEGER;
  BEGIN
    IF p_pesel IS NULL THEN
      RETURN FALSE;
    END IF;
    IF p_pesel < 0 THEN
      RETURN FALSE;
    END IF;
    l_length := length(to_char(p_pesel));
    IF l_length <> 11 THEN
      RETURN FALSE;
    END IF;
    RETURN TRUE;
  END validate_pesel_basic;
  -------------------------------------------------
  -------------------------------------------------
  FUNCTION validate_control_digit(
     p_pesel NUMBER)
    RETURN BOOLEAN
  IS
    l_control_sum PLS_INTEGER;
    l_pesel VARCHAR2(11);
  BEGIN
    l_pesel := TO_CHAR(p_pesel);    
    l_control_sum := get_digit(l_pesel,1);
    l_control_sum := l_control_sum + 3*get_digit(l_pesel,2);
    l_control_sum := l_control_sum + 7*get_digit(l_pesel,3);
    l_control_sum := l_control_sum + 9*get_digit(l_pesel,4);
    l_control_sum := l_control_sum + get_digit(l_pesel,5);
    l_control_sum := l_control_sum + 3*get_digit(l_pesel,6);
    l_control_sum := l_control_sum + 7*get_digit(l_pesel,7);
    l_control_sum := l_control_sum + 9*get_digit(l_pesel,8);
    l_control_sum := l_control_sum + get_digit(l_pesel,9);
    l_control_sum := l_control_sum + 3*get_digit(l_pesel,10);
    l_control_sum := l_control_sum + get_digit(l_pesel,11);
    RETURN l_control_sum mod 10 = 0;
  END validate_control_digit;
  -------------------------------------------------
  -------------------------------------------------
  FUNCTION get_digit(p_number VARCHAR2,p_position PLS_INTEGER)
  RETURN PLS_INTEGER
  IS
  BEGIN
    RETURN TO_NUMBER(SUBSTR(p_number,p_position,1));
  END get_digit;

END PESEL_UTIL;

/
