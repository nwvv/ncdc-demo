package com.pw.demo.book.web.view.backing;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.pw.demo.book.exception.BookDemoServiceException;
import com.pw.demo.book.model.dto.BookDTO;

/**
 * Managed Bean backing Book list page.
 * 
 * @author Przemyslaw Walat
 *
 */
@ManagedBean(name = "listBooksBean")
@SessionScoped
public class ViewBooksManagedBean extends AbstractBookBackingBean {

	public List<BookDTO> getBooks() throws BookDemoServiceException {
		return getBookService().getBooks(null);
	}

}
