package com.pw.demo.book.web.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.plugins.validation.hibernate.ValidateRequest;

import com.pw.demo.book.exception.BookDemoServiceException;
import com.pw.demo.book.logging.InvocationLogging;
import com.pw.demo.book.logging.PerformanceLogging;
import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.services.book.BookRepositoryService;
import com.pw.demo.book.services.common.PaginationData;

@Path("/book")
@ValidateRequest
@PerformanceLogging
@InvocationLogging
public class BookRESTService {

	@EJB
	private BookRepositoryService bookService;

	/**
	 * @return
	 * @throws BookDemoServiceException
	 */
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/getAllBooks")
	public List<BookDTO> getBooks(
			@QueryParam("booksPerPage") Integer booksPerPage,
			@QueryParam("pageNumber") Integer pageNumber)
			throws BookDemoServiceException {
		PaginationData pagination = null;
		if (booksPerPage != null) {
			pagination = new PaginationData(booksPerPage, pageNumber);
		}
		return bookService.getBooks(pagination);
	}

	/**
	 * @param book
	 * @see com.pw.demo.book.services.book.BookRepositoryService#addBook(com.pw.demo.book.model.dto.BookDTO)
	 */
	@Consumes(MediaType.APPLICATION_JSON)
	@POST
	@Path("/addBook")
	public void addBook(@Valid BookDTO book) {
		bookService.addBook(book);
	}

}
