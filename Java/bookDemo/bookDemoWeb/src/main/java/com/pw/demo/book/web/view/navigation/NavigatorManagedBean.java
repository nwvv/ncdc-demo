package com.pw.demo.book.web.view.navigation;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "navigator")
@ApplicationScoped
public class NavigatorManagedBean {

	public String getAddBook() {
		return ViewNames.ADD_BOOK;
	}
}
