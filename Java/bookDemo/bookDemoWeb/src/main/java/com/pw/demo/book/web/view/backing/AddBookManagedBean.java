package com.pw.demo.book.web.view.backing;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.web.view.navigation.ViewNames;

/**
 * Managed Bean backing Book add page.
 * 
 * @author Przemyslaw Walat
 *
 */
@ManagedBean(name = "addBookBean")
@SessionScoped
public class AddBookManagedBean extends AbstractBookBackingBean {

	private BookDTO book;

	public String getAuthor() {
		return getBook().getAuthor();
	}

	public String getISBN() {
		return getBook().getISBN();
	}

	public String getTitle() {
		return getBook().getTitle();
	}

	public void setAuthor(String author) {
		getBook().setAuthor(author);
	}

	public void setISBN(String iSBN) {
		getBook().setISBN(iSBN);
	}

	public void setTitle(String title) {
		getBook().setTitle(title);
	}

	public String addBook() {
		getBookService().addBook(getBook());
		book = null;
		return ViewNames.LIST_BOOKS;
	}

	private BookDTO getBook() {
		if (book == null) {
			book = new BookDTO();
		}
		return book;
	}

}
