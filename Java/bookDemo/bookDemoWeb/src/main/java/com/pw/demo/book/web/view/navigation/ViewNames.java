package com.pw.demo.book.web.view.navigation;

public interface ViewNames {
	String ADD_BOOK = "add-book";
	String LIST_BOOKS = "list-books";
}
