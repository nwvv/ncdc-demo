package com.pw.demo.book.web.view.backing;

import javax.ejb.EJB;

import com.pw.demo.book.services.book.BookRepositoryService;

public abstract class AbstractBookBackingBean {

	@EJB
	private BookRepositoryService bookService;

	public AbstractBookBackingBean() {
		super();
	}

	protected BookRepositoryService getBookService() {
		return bookService;
	}

}