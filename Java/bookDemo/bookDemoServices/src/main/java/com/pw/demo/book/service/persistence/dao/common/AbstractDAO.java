package com.pw.demo.book.service.persistence.dao.common;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pw.demo.book.service.BookDemoConstants;
import com.pw.demo.book.service.persistence.util.JPAQueryUtil;
import com.pw.demo.book.services.common.PaginationData;

/**
 * Abstract base class for Data Access Objects.
 * 
 * @param <EntityClass>
 *            Class of the Entity the DAO will mainly deal with.
 * @author Przemyslaw Walat
 *
 */
public class AbstractDAO<EntityClass> implements DataAccessObject<EntityClass> {

	@PersistenceContext(unitName = BookDemoConstants.PERSISTENCE_UNIT_NAME)
	private EntityManager em;

	private final Class<EntityClass> entityClass;
	private final String getAllEntitiesQueryName;

	/**
	 * 
	 * Constructor for AbstractDAO
	 */
	public AbstractDAO(Class<EntityClass> entityClass,
			String getAllEntitiesQueryName) {
		this.entityClass = entityClass;
		this.getAllEntitiesQueryName = getAllEntitiesQueryName;
	}

	/**
	 * Applies paging to the query, based on supplied {@link PaginationData}
	 * 
	 * @param query
	 *            - the Query to apply paging to
	 * @param paginationData
	 *            - {@link PaginationData} to apply
	 */
	protected void applyPaging(Query query, PaginationData paginationData) {
		JPAQueryUtil.applyPaging(query, paginationData);
	}

	/**
	 * Create a typed named query parameterized for returning this DAO's entity.
	 * 
	 * @param name
	 *            name of the {@link NamedQuery}
	 * @return
	 */
	protected TypedQuery<EntityClass> createTypedQueryForEntity(String name) {
		TypedQuery<EntityClass> query = getEm().createNamedQuery(name,
				entityClass);
		return query;
	}

	@Override
	public void delete(EntityClass entity) {
		getEm().remove(entity);
	}

	@Override
	public EntityClass get(Long id) {
		return getEm().find(entityClass, id);
	}

	@Override
	public List<EntityClass> getAll(PaginationData pagingData) {
		return getAll(pagingData, false);
	}

	@Override
	public List<EntityClass> getAll(PaginationData paginationData,
			Boolean readOnly) {
		TypedQuery<EntityClass> query = createTypedQueryForEntity(getAllEntitiesQueryName);
		if (readOnly) {
			optimizeForReadOnlyAccess(query);
		}
		if (paginationData != null) {
			applyPaging(query, paginationData);
		}
		return query.getResultList();
	}

	/**
	 * Get the {@link EntityManager}
	 * 
	 * @return
	 */
	protected EntityManager getEm() {
		return em;
	}

	@Override
	public EntityClass getReference(Long id) {
		return getEm().getReference(entityClass, id);
	}

	@Override
	public EntityClass merge(EntityClass message) {
		return getEm().merge(message);
	}

	/**
	 * Optimize a query for read only access. It will run faster and the
	 * transaction it is bound to will commit faster, but the drawback is that
	 * any changes made to the entities returned by the query will not be
	 * reflected in the database.
	 * 
	 * @param query
	 */
	protected void optimizeForReadOnlyAccess(TypedQuery<EntityClass> query) {
		JPAQueryUtil.applyReadOnlyHint(query);
		JPAQueryUtil.setFlushModeToCommit(query);
	}

	@Override
	public void persist(EntityClass message) {
		getEm().persist(message);
	}

}