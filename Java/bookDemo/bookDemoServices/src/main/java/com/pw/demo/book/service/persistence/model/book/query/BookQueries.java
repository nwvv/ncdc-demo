package com.pw.demo.book.service.persistence.model.book.query;

import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;

/**
 * Contains JPQL Queries related to {@link BookEntity} entity.
 * 
 * @author Przemyslaw Walat
 *
 */
public interface BookQueries {

	String ENTITY_NAME = "BOOK";

	/**
	 * Fetch all {@link BookEntity} entities
	 *
	 */
	interface GetAll {
		String NAME = ENTITY_NAME + ".getAll";
		String QUERY = "SELECT b FROM BookEntity b ORDER BY b.author,b.title";
	}


}
