package com.pw.demo.book.service.mapper;

import java.util.List;

/**
 * Base Mapper interface, maps between entities and DTOs
 * 
 * @author Przemyslaw Walat
 *
 * @param <EntityType>
 *            class of the entity
 * @param <DTOType>
 *            class of the DTO
 * 
 */
public interface Mapper<EntityType, DTOType> {

	EntityType toEntity(DTOType dto);

	List<EntityType> toEntity(List<DTOType> dtos);

	DTOType toDTO(EntityType entity);

	List<DTOType> toDTO(List<EntityType> entities);

}
