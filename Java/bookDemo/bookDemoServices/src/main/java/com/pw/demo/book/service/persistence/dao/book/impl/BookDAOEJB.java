package com.pw.demo.book.service.persistence.dao.book.impl;

import javax.ejb.Stateless;

import com.pw.demo.book.logging.InvocationLogging;
import com.pw.demo.book.logging.PerformanceLogging;
import com.pw.demo.book.service.persistence.dao.book.BookDAO;
import com.pw.demo.book.service.persistence.dao.common.AbstractDAO;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;
import com.pw.demo.book.service.persistence.model.book.query.BookQueries;

/**
 * EJB implementation of MessageDAO
 * 
 * @author Przemysław Walat
 *
 */
@Stateless
public class BookDAOEJB extends AbstractDAO<BookEntity> implements BookDAO {

	/**
	 * 
	 * Constructor for BookDAOEJB
	 */
	public BookDAOEJB() {
		super(BookEntity.class, BookQueries.GetAll.NAME);
	}

}
