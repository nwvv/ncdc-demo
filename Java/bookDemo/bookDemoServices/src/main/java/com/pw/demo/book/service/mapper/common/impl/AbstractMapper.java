package com.pw.demo.book.service.mapper.common.impl;

import java.util.ArrayList;
import java.util.List;

import com.pw.demo.book.service.mapper.Mapper;

/**
 * Base abstract class for Entity<>DTO Mappers. It is capable of mapping in both
 * ways.
 * 
 * @author Przemyslaw Walat
 *
 * @param <EntityType>
 *            type of the enity supported by the mapper
 * @param <DTOType>
 *            type of the DTO supported by the mapper
 */
public abstract class AbstractMapper<EntityType, DTOType> implements
		Mapper<EntityType, DTOType> {

	@Override
	public DTOType toDTO(EntityType entity) {
		DTOType dto = createDTOInstance();
		fillDTO(dto, entity);
		return dto;
	}

	@Override
	public EntityType toEntity(DTOType dto) {
		EntityType entity = createEntityInstance();
		fillEntity(entity, dto);
		return entity;
	}

	protected abstract DTOType createDTOInstance();

	protected abstract EntityType createEntityInstance();

	protected abstract void fillDTO(DTOType dto, EntityType entity);

	protected abstract void fillEntity(EntityType entity, DTOType dto);

	@Override
	public List<EntityType> toEntity(List<DTOType> dtos) {
		List<EntityType> entities = new ArrayList<EntityType>();
		for (DTOType dto : dtos) {
			entities.add(toEntity(dto));
		}
		return entities;
	}

	@Override
	public List<DTOType> toDTO(List<EntityType> entities) {
		List<DTOType> dtos = new ArrayList<DTOType>();
		for (EntityType entity : entities) {
			dtos.add(toDTO(entity));
		}
		return dtos;
	}

}