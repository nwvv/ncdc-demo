package com.pw.demo.book.service.persistence.dao.book;

import java.util.List;

import com.pw.demo.book.service.persistence.dao.common.DataAccessObject;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;
import com.pw.demo.book.services.common.PaginationData;

/**
 * Data Access Object for {@link BookEntity}
 * 
 * @author Przemyslaw Walat
 *
 */
public interface BookDAO extends DataAccessObject<BookEntity> {
	
	
	/**
	 * Get all {@link BookEntity}s
	 * 
	 * @param pagingData
	 *            {@link PaginationData} for the request 
	 * 
	 * @return the list of all {@link BookEntity}s
	 */
	List<BookEntity> getAll(PaginationData pagingData);

	/**
	 * Get all {@link BookEntity}s
	 * 
	 * @param pagingData
	 *            {@link PaginationData} for the request
	 * 
	 * @param readOnly
	 *            specify whether {@link BookEntity} instances returned should be read
	 *            only, if set to true, any changes made to entities returned
	 *            <b>will not</> be reflected in the database
	 * @return the list of all {@link BookEntity}s
	 */
	List<BookEntity> getAll(PaginationData pagingData, Boolean readOnly);

}