package com.pw.demo.book.service.interceptor.impl.common;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Abstract base class for interceptors whose main purpose is logging.
 * 
 * @author Przemyslaw Walat
 *
 */
public abstract class AbstractLoggingInterceptor {

	private final Logger logger = Logger.getLogger(getClass().getName());
	private final String prefix;
	private final Level defaultLevel;

	public AbstractLoggingInterceptor(String prefix, Level defaultLevel) {
		this.prefix = prefix;
		this.defaultLevel = defaultLevel;
	}

	/**
	 * Checks whether the interceptor's default logging level is loggable.
	 * 
	 * @return true if the level is loggable, false otherwise
	 */
	protected boolean defaultLoggingLevelLoggable() {
		return logLevelEnabled(getDefaultLevel());
	}

	protected String getPrefix() {
		return prefix;
	}

	/**
	 * Writes to log with the Level specified.
	 * 
	 * @param level
	 *            logging {@link Level}
	 * @param message
	 *            - message to write, the log's prefix will be appended to the
	 *            message
	 */
	protected void log(Level level, String message) {
		getLogger().log(level, getPrefix() + message);
	}

	/**
	 * Writes to log with the default Level of the interceptor.
	 * 
	 * @param message
	 *            - message to write, the log's prefix will be appended to the
	 *            message
	 */
	protected void log(String message) {
		log(getDefaultLevel(), message);
	}

	/**
	 * Logs an exception at logging {@link Level} level.SEVERE.
	 * 
	 * @param e
	 *            - the exception to log
	 * @param message
	 *            - the message to attach to the log.
	 */
	protected void logError(Exception e, String message) {
		getLogger().log(Level.SEVERE, getPrefix() + message, e);
	}

	protected Level getDefaultLevel() {
		return defaultLevel;
	}

	protected Logger getLogger() {
		return logger;
	}

	protected boolean logLevelEnabled(Level level) {
		return getLogger().isLoggable(level);
	}

}