package com.pw.demo.book.service.interceptor.impl.invocation;

import java.util.logging.Level;

import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.pw.demo.book.logging.InvocationLogging;
import com.pw.demo.book.service.interceptor.impl.common.AbstractAroundInvokeLoggingInterceptor;

/**
 * Logs the name of the method being called. Very useful during debugging, can
 * be attached to specific methods of whole classes along a concrete use case
 * invocation path. Only processes invocations when at least log level INFO is
 * loggable. Starts to log invocation parameters when log level FINE is
 * enabled.
 * 
 * @author Przemyslaw Walat
 * 
 */

@InvocationLogging
@Interceptor
public class MethodInvocationLoggingInterceptor extends
		AbstractAroundInvokeLoggingInterceptor {

	public static String LOG_PREFIX = "METHOD_TRACE_LOG:\t";
	private static final Level parameterLoggingLevel = Level.FINE;

	public MethodInvocationLoggingInterceptor() {
		super(LOG_PREFIX, Level.INFO);
	}

	public void logError(Exception e) {
		logError(e, "Exception while tracing method call.");
	}

	@Override
	protected Object handleAroundInvoke(InvocationContext context)
			throws Exception {
		String methodName = getMethodName(context);
		logEntry(methodName);
		if (logLevelEnabled(parameterLoggingLevel)) {
			logParameters(context);
		}
		Object result = context.proceed();
		logExit(methodName);
		return result;
	}

	private void logEntry(String methodName) {
		try {
			if (methodName != null) {
				logEntryMessage(methodName);
			}
		} catch (Exception e) {
			logError(e);
		}
	}

	private void logEntryMessage(String methodNamey) {
		log("Entering " + methodNamey);
	}

	private void logExit(String methodName) {
		try {
			if (methodName != null) {
				logExitMessage(methodName);
			}
		} catch (Exception e) {
			logError(e);
		}
	}

	private void logExitMessage(String methodName) {
		log("Exiting " + methodName);
	}

	private void logParameters(InvocationContext context) {
		Object[] parameters = context.getParameters();
		StringBuilder message = new StringBuilder(
				"Arguments passed in to the method:\n");
		for (Object parameter : parameters) {
			String toString = ToStringBuilder.reflectionToString(parameter,
					ToStringStyle.SHORT_PREFIX_STYLE,true,Object.class);
			message.append(toString).append("\n");
		}
		log(parameterLoggingLevel, message.toString());
	}

}
