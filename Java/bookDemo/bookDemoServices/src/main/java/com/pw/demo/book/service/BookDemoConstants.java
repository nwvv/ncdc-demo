package com.pw.demo.book.service;

/**
 * Constants of Book Demo Application
 * 
 * @author Przemyslaw Walat
 *
 */
public interface BookDemoConstants {

	String PERSISTENCE_UNIT_NAME = "book-demo";
}
