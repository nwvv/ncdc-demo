package com.pw.demo.book.service.persistence.util;

import javax.persistence.FlushModeType;
import javax.persistence.Query;

import com.pw.demo.book.logging.InvocationLogging;
import com.pw.demo.book.services.common.PaginationData;

/**
 * Provides utility methods for JPA queries.
 * 
 * @author Przemyslaw Walat
 *
 */
public class JPAQueryUtil {

	/**
	 * Apply paging to the query
	 * 
	 * @param query
	 *            - the {@link Query} to apply paging to
	 * @param itemsPerPage
	 *            - desired number of items per page
	 * @param firstItem
	 *            - the first desired item
	 */
	public static void applyPaging(Query query, Integer itemsPerPage,
			Integer firstItem) {
		query.setMaxResults(itemsPerPage);
		query.setFirstResult(firstItem);
	}

	/**
	 * Apply paging to the query
	 * 
	 * @param query
	 *            - the {@link Query} to apply paging to
	 * @param paginationData
	 *            - {@link PaginationData} describing the desired pagination
	 */
	public static void applyPaging(Query query, PaginationData paginationData) {
		applyPaging(query, paginationData.getItemsPerPage(),
				paginationData.getFirstItem());
	}

	/**
	 * Applies a read-only hint, telling persistence not to perform dirty-checks
	 * entity instances returned by the service. If this hint is applied, any
	 * changes made to entities returned by the query will not be reflected in
	 * the database, but the transaction will be commited much faster due to
	 * lack of dirty checking.
	 * 
	 * @param query
	 *            the {@link Query} to apply the hint to
	 */
	public static void applyReadOnlyHint(Query query) {
		query.setHint(READ_ONLY_HINT_NAME, Boolean.TRUE);
	}

	/**
	 * Sets {@link FlushModeType} of the {@link Query} to COMMIT to speed up
	 * execution. The drawback is that there is a risk of fetching stale data.
	 * 
	 * @param query
	 */
	public static void setFlushModeToCommit(Query query) {
		query.setFlushMode(FlushModeType.COMMIT);
	}

	public static final String READ_ONLY_HINT_NAME = "org.hibernate.readOnly";

}
