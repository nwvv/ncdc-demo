package com.pw.demo.book.service.mapper.book.impl;

import javax.ejb.Stateless;

import com.pw.demo.book.logging.InvocationLogging;
import com.pw.demo.book.model.Book;
import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.service.mapper.book.BookMapper;
import com.pw.demo.book.service.mapper.common.impl.AbstractInterfaceBasedMapper;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;

/**
 * Mapper for book entities and DTOs
 * 
 * @author Przemyslaw Walat
 *
 */
@InvocationLogging
@Stateless
public class BookMapperEJB extends
		AbstractInterfaceBasedMapper<Book, BookEntity, BookDTO> implements
		BookMapper {

	/**
	 * The default constructor.
	 */
	public BookMapperEJB() {
		super(BookEntity.class, BookDTO.class);
	}

	@Override	
	protected void fillTargetInstance(Book source, Book target) {
		target.setAuthor(source.getAuthor());
		target.setId(source.getId());
		target.setISBN(source.getISBN());
		target.setTitle(source.getTitle());
	}

}
