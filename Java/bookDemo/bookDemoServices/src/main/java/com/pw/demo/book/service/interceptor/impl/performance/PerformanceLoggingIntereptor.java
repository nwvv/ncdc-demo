package com.pw.demo.book.service.interceptor.impl.performance;


import java.util.logging.Level;

import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;




import com.pw.demo.book.logging.PerformanceLogging;
import com.pw.demo.book.service.interceptor.impl.common.AbstractAroundInvokeLoggingInterceptor;

/**
 * Logs the name of the method being called and time spent on its invocation. A
 * simplistic profiler. Can be be attached to specific methods or whole classes.
 * Only processes invocations when at least log level FINE is loggable.
 * 
 * @author Przemyslaw Walat
 * 
 */
@PerformanceLogging @Interceptor
public class PerformanceLoggingIntereptor extends
		AbstractAroundInvokeLoggingInterceptor {

	public static String LOG_PREFIX = "PERFORMANCE_TRACE_LOG:\t";

	public PerformanceLoggingIntereptor() {
		super(LOG_PREFIX, Level.FINE);
	}

	@Override
	protected Object handleAroundInvoke(InvocationContext context)
			throws Exception {
		long startTime = System.currentTimeMillis();
		Object result = context.proceed();
		logEndTime(startTime, context);
		return result;
	}

	private void logEndTime(long startTime, InvocationContext context) {
		String methodName = getMethodName(context);
		long endTime = System.currentTimeMillis();
		long total = endTime - startTime;
		log(methodName + " completed in " + total + " ms.");
	}
}
