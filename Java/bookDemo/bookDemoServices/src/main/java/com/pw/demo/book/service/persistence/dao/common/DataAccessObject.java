package com.pw.demo.book.service.persistence.dao.common;

import java.util.List;

import com.pw.demo.book.services.common.PaginationData;

/**
 * A Base DAO providing basic CRUD operations for a specific Entity
 * 
 * 
 * @param <EntityClass>
 *            class of the entity supported by this {@link DataAccessObject}
 * @author Przemyslaw Walat
 */
public interface DataAccessObject<EntityClass> {

	/**
	 * Delete the Entity.
	 * 
	 * @param entity
	 */
	void delete(EntityClass entity);

	/**
	 * Fetch the Entity
	 * 
	 * @param id
	 * @return
	 */
	EntityClass get(Long id);

	/**
	 * Get all EntityClass instances
	 * 
	 * @param pagingData
	 *            {@link PaginationData} for the request, if it is null, no
	 *            paging is applied
	 * 
	 * @return the list of all EntityClass instances
	 */
	List<EntityClass> getAll(PaginationData pagingData);

	/**
	 * Get all EntityClass instances
	 * 
	 * @param pagingData
	 *            {@link PaginationData} for the request, if it is null, no
	 *            paging is applied
	 * 
	 * @param readOnly
	 *            specify whether EntityClass instances returned should be read
	 *            only, if set to true, any changes made to entities returned
	 *            <b>will not</> be reflected in the database
	 * @return the list of all EntityClass instances
	 */
	List<EntityClass> getAll(PaginationData pagingData, Boolean readOnly);

	/**
	 * Get the entity reference instead of the whole Entity
	 * 
	 * @param id
	 * @return
	 */
	EntityClass getReference(Long id);

	/**
	 * Merge the Entity
	 * 
	 * @param message
	 * @return
	 */
	EntityClass merge(EntityClass message);

	/**
	 * Persist the Entity
	 * 
	 * @param message
	 */
	void persist(EntityClass message);

}