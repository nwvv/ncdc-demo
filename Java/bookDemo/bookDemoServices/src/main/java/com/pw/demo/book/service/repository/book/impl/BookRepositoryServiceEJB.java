package com.pw.demo.book.service.repository.book.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pw.demo.book.exception.BookDemoServiceException;
import com.pw.demo.book.logging.InvocationLogging;
import com.pw.demo.book.logging.PerformanceLogging;
import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.service.mapper.book.BookMapper;
import com.pw.demo.book.service.persistence.dao.book.BookDAO;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;
import com.pw.demo.book.services.book.BookRepositoryService;
import com.pw.demo.book.services.common.PaginationData;

/**
 * EJB Implementation of {@link BookRepositoryService}
 * 
 * @author Przemyslaw Walat
 *
 */
@Stateless
@PerformanceLogging
@InvocationLogging
public class BookRepositoryServiceEJB implements BookRepositoryService {

	@EJB
	private BookDAO dao;

	@EJB
	private BookMapper mapper;

	@Override
	public List<BookDTO> getBooks(PaginationData pagingData)
			throws BookDemoServiceException {
		List<BookEntity> entities = dao.getAll(pagingData, true);
		List<BookDTO> dtos = mapper.toDTO(entities);
		return dtos;
	}

	@Override
	public void addBook(BookDTO book) {
		BookEntity entity = mapper.toEntity(book);
		dao.persist(entity);
	}

}
