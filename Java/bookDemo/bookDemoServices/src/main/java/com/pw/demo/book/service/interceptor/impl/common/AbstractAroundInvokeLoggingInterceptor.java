package com.pw.demo.book.service.interceptor.impl.common;

import java.util.logging.Level;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 * Abstract base class for AroundInvoke interceptors dealing with logging.
 * @author Przemyslaw Walat
 *
 */
public abstract class AbstractAroundInvokeLoggingInterceptor extends
		AbstractLoggingInterceptor {

	public AbstractAroundInvokeLoggingInterceptor(String prefix,
			Level defaultLevel) {
		super(prefix, defaultLevel);
	}

	@AroundInvoke
	public Object aroundInvoke(InvocationContext context) throws Exception {
		if (defaultLoggingLevelLoggable()) {
			return handleAroundInvoke(context);
		} else {
			return context.proceed();
		}
	}

	protected String getMethodName(InvocationContext context) {
		String methodName = null;
		try {
			String targetClassName = context.getTarget().getClass().getName();
			methodName = targetClassName+"."+context.getMethod().getName();
		} catch (Exception e) {
			logError(e, "Exception getting method name");
		}
		return methodName;
	}

	protected abstract Object handleAroundInvoke(InvocationContext context)
			throws Exception;

}