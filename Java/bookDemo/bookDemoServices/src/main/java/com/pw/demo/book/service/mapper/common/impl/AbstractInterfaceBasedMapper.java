package com.pw.demo.book.service.mapper.common.impl;

import com.pw.demo.book.exception.BookDemoSystemException;

/**
 * Abstract base implementation for DTO<>Entity mappers basing on the assumption
 * that both the enity and dto implement the same interface, which can be used
 * during mapping thus allowing to implement it only once.
 * 
 * @author nHw
 *
 * @param <CommonInterface>
 */
public abstract class AbstractInterfaceBasedMapper<CommonInterface, EntityType extends CommonInterface, DTOType extends CommonInterface>
		extends AbstractMapper<EntityType, DTOType> {
	private final Class<EntityType> entityClass;
	private final Class<DTOType> dtoClass;

	/**
	 * The constructor for {@link AbstractInterfaceBasedMapper}
	 * 
	 * @param entityClass
	 *            Class of the object this mapper is going to fill.
	 */
	public AbstractInterfaceBasedMapper(Class<EntityType> entityClass,
			Class<DTOType> dtoClass) {
		this.entityClass = entityClass;
		this.dtoClass = dtoClass;
	}

	@Override
	protected DTOType createDTOInstance() {
		return createInstanceUsingDefaultConstuctor(dtoClass);
	}

	@Override
	protected EntityType createEntityInstance() {
		return createInstanceUsingDefaultConstuctor(entityClass);
	}

	/**
	 * Creates an the target instance. The target's class must have a default
	 * constructor. If it does not, this method has to be overriden.
	 * 
	 * @return A new, empty instance of the target class.
	 */
	protected <T> T createInstanceUsingDefaultConstuctor(Class<T> instanceClass) {
		try {
			return instanceClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new BookDemoSystemException(
					"Unable to instantiate result instance. If the target class does not have a default constructor, the extending mappers must override creataResultInstance",
					e);
		}
	}

	@Override
	protected void fillDTO(DTOType dto, EntityType entity) {
		fillTargetInstance(entity, dto);
	}

	@Override
	protected void fillEntity(EntityType entity, DTOType dto) {
		fillTargetInstance(dto, entity);
	}

	/**
	 * Fill target's fields using values from source.
	 * 
	 * @param source
	 * @param target
	 */
	protected abstract void fillTargetInstance(CommonInterface source,
			CommonInterface target);

}
