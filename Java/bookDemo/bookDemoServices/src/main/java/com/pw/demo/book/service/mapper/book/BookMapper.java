package com.pw.demo.book.service.mapper.book;

import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.service.mapper.Mapper;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;

public interface BookMapper extends Mapper<BookEntity, BookDTO> {

}
