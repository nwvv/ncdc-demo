package com.pw.demo.book.mapper.book;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.pw.demo.book.common.BookUtil;
import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.service.mapper.book.impl.BookMapperEJB;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;

public class BookMapperTest {
	
	private BookMapperEJB mapper;

	@Before
	public void setUp() throws Exception {
		mapper = new BookMapperEJB();
	}
	
	@Test
	public void test() {
		BookEntity initialEntity = BookUtil.createBook(1);
		BookDTO dto = mapper.toDTO(initialEntity);
		BookEntity entityFromDTO = mapper.toEntity(dto);
		assertEquals(initialEntity,entityFromDTO);
		assertNotSame(initialEntity,entityFromDTO);
	}

}
