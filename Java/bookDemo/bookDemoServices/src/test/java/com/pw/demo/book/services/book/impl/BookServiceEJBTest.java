package com.pw.demo.book.services.book.impl;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pw.demo.book.common.AbstractEntityTestPlatform;
import com.pw.demo.book.common.BookUtil;
import com.pw.demo.book.service.persistence.dao.book.BookDAO;
import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;
import com.pw.demo.book.services.common.PaginationData;

@RunWith(Arquillian.class)
public class BookServiceEJBTest extends AbstractEntityTestPlatform {

	private static final int TOTAL_BOOK_COUNT = 10;
	private static final int ITEMS_PER_PAGE = 5;

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class, "test.war")
				.addPackages(true, "com.pw.demo.book")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("wildfly-ds.xml")
				.addAsResource("test-persistence.xml",
						"META-INF/persistence.xml");
	}

	@Inject
	private BookDAO bookDAO;

	@Before
	public void setUp() throws Exception {
		clearData();
		initializeData();
	}

	@After
	public void tearDown() {
		try {
			commitTransaction();
		} catch (Exception e) {
			// We need to try to commit because failed tests might not do
			// that.
		}
	}

	@Test
	public void testListAllBooks() throws Exception {
		beginTransaction();
		List<BookEntity> books = bookDAO.getAll(null);
		assertNotNull(books);
		assertEquals(TOTAL_BOOK_COUNT, books.size());
		commitTransaction();
	}

	@Test
	public void testListAllBooksPagination() throws Exception {
		beginTransaction();
		List<BookEntity> booksPageOne = bookDAO.getAll(new PaginationData(
				ITEMS_PER_PAGE, 1));
		assertNotNull(booksPageOne);
		assertEquals(ITEMS_PER_PAGE, booksPageOne.size());
		List<BookEntity> booksPageTwo = bookDAO.getAll(new PaginationData(
				ITEMS_PER_PAGE, 2));
		assertNotNull(booksPageTwo);
		assertEquals(ITEMS_PER_PAGE, booksPageTwo.size());
		for (BookEntity book : booksPageTwo) {
			assertFalse(booksPageOne.contains(book));
		}
		for (BookEntity book : booksPageOne) {
			assertFalse(booksPageTwo.contains(book));
		}
		commitTransaction();
	}

	private void clearData() throws Exception {
		beginAndJoinTransaction();
		getEntityManager().createQuery("delete from BookEntity")
				.executeUpdate();
		commitTransaction();
	}

	private void initializeData() throws Exception {
		beginTransaction();
		for (int i = 0; i < TOTAL_BOOK_COUNT; i++) {
			persistBook(i);
		}
		commitTransaction();
	}

	private void persistBook(Integer number) {
		getEntityManager().persist(BookUtil.createBook(number));
	}

}
