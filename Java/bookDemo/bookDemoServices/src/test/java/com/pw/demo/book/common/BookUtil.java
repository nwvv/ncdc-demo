package com.pw.demo.book.common;

import com.pw.demo.book.service.persistence.model.book.entity.BookEntity;

public class BookUtil {

	public static BookEntity createBook(Integer number) {
		String formattedNumber = String.format("%08d", number);
		BookEntity book = new BookEntity();
		book.setAuthor("Author " + formattedNumber);
		book.setTitle("Title " + formattedNumber);
		book.setISBN("ISBN " + formattedNumber);
		return book;
	}

}
