package com.pw.demo.book.exception;

/**
 * Application-specific exception, thrown in case an unrecoverable exception
 * occurs.
 * 
 * @author Przemyslaw Walat
 *
 */
public class BookDemoSystemException extends RuntimeException {

	private static final long serialVersionUID = -6161844780621670340L;

	/**
	 * 
	 * Constructor for {@link BookDemoSystemException}
	 * 
	 * @param message
	 *            - The message describing the error that occured
	 */
	public BookDemoSystemException(String message) {
		super(message);
	}

	/**
	 * Constructor for {@link BookDemoSystemException}
	 * 
	 * @param message
	 *            - The message describing the error that occured
	 * @param cause
	 *            - The underlying cause
	 */
	public BookDemoSystemException(String message, Throwable cause) {
		super(message, cause);
	}

}
