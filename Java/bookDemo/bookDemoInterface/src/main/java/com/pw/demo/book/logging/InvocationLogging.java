package com.pw.demo.book.logging;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.interceptor.InterceptorBinding;

/**
 * An annotation used to enable invocation logging for a class or a specific method. 
 * If it is specified for a class, the annotation will be applied to all of its public methods.
 * Binds its target to an interceptor which will log time spent in a method.
 * 
 * @author Przemyslaw Walat
 */
@Retention(RUNTIME)
@Target({ METHOD, TYPE })
@InterceptorBinding
public @interface InvocationLogging {

}
