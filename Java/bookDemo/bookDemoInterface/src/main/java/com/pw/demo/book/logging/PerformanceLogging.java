package com.pw.demo.book.logging;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.interceptor.InterceptorBinding;

/**
 * An annotation used to enable method invocation logging on a class or method.
 * If it is specified for a class, all to all of its methods. At LogLevel FINE
 * tracks when the method is entered and existed. At LogLevel FINEST also tracks
 * arguments passed to the method.
 * 
 * @author Przemyslaw Walat
 */
@Retention(RUNTIME)
@Target({ METHOD, TYPE })
@InterceptorBinding
public @interface PerformanceLogging {

}
