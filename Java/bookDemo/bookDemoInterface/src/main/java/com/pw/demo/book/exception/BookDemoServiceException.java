package com.pw.demo.book.exception;

/**
 * Application-specific exception. Thrown when application logic-related
 * exceptions occur.
 * 
 * @author Przemyslaw Walat
 *
 */
public class BookDemoServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7274444153617496348L;

	/**
	 * 
	 * Constructor for {@link BookDemoServiceException}
	 * 
	 * @param message
	 *            - The message describing the error that occured
	 */
	public BookDemoServiceException(String message) {
		super(message);
	}

	/**
	 * Constructor for {@link BookDemoServiceException}
	 * 
	 * @param message
	 *            - The message describing the error that occured
	 * @param cause
	 *            - The underlying cause
	 */
	public BookDemoServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}
