package com.pw.demo.book.model;

import java.io.Serializable;

public interface Book extends Serializable {

	/**
	 * @return the author
	 */
	String getAuthor();

	/**
	 * @return the id
	 */
	Long getId();

	/**
	 * @return the iSBN
	 */
	String getISBN();

	/**
	 * @return the title
	 */
	String getTitle();

	/**
	 * @param author
	 *            the author to set
	 */
	void setAuthor(String author);

	/**
	 * @param id
	 *            the id to set
	 */
	void setId(Long id);

	/**
	 * @param iSBN
	 *            the iSBN to set
	 */
	void setISBN(String iSBN);

	/**
	 * @param title
	 *            the title to set
	 */
	void setTitle(String title);

}