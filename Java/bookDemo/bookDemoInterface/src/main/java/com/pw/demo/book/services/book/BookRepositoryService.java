package com.pw.demo.book.services.book;

import java.util.List;

import com.pw.demo.book.exception.BookDemoServiceException;
import com.pw.demo.book.model.dto.BookDTO;
import com.pw.demo.book.services.common.PaginationData;

/**
 * The service allows for listing messages based on filter set in this service's
 * configuration.
 * 
 * @author Przemyslaw Walat
 */
public interface BookRepositoryService {

	/**
	 * Get a list of all Books.
	 * 
	 * @return A List containing all {@link BookDTO}s. e.
	 * @throws BookDemoServiceException
	 */
	List<BookDTO> getBooks(PaginationData pagingData)
			throws BookDemoServiceException;

	/**
	 * Adds a new Book.
	 * 
	 * @param book
	 *            - the {@link BookDTO} represtenting the book to add
	 */
	void addBook(BookDTO book);

}