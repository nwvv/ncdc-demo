package com.pw.demo.book.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.pw.demo.book.model.Book;

/**
 * Book Data Transfer Object.
 * 
 * @author Przemyslaw Walat
 *
 */
public class BookDTO implements Book {

	private static final long serialVersionUID = 2621000228352535276L;

	private Long id;

	@NotNull
	@Pattern(regexp="(A.* .*)|(.* A.*)")
	private String author;

	@NotNull
	private String title;

	@NotNull
	private String ISBN;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookDTO other = (BookDTO) obj;
		if (ISBN == null) {
			if (other.ISBN != null)
				return false;
		} else if (!ISBN.equals(other.ISBN))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#getAuthor()
	 */
	@Override
	public String getAuthor() {
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#getISBN()
	 */
	@Override
	public String getISBN() {
		return ISBN;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ISBN == null) ? 0 : ISBN.hashCode());
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#setAuthor(java.lang.String)
	 */
	@Override
	public void setAuthor(String author) {
		this.author = author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#setISBN(java.lang.String)
	 */
	@Override
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pw.demo.book.dto.Book#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
