package com.pw.demo.book.services.common;

import java.io.Serializable;

/**
 * Specifies request pagination details. <b>Does not allow null values</b>
 * 
 * @author Przemyslaw Walat
 */
public class PaginationData implements Serializable {
	private static final long serialVersionUID = 8704465364346657380L;
	private static final String ITEMS_PER_PAGE_FIELD_NAME = "Items per page";
	private static final String PAGE_NUMBER_FIELD_NAME = "Page number";
	private Integer itemsPerPage = 20;
	private Integer pageNumber = 1;

	/**
	 * Constructor
	 * 
	 * @param itemsPerPage
	 *            - the desired number of items per page, must be a natural
	 *            number
	 * @param pageNumber
	 *            - the desired page number, must be a natural number
	 */
	public PaginationData(Integer itemsPerPage, Integer pageNumber) {
		setItemsPerPage(itemsPerPage);
		setPageNumber(pageNumber);
	}

	/**
	 * Get the ordinal number of the first item to retrieve, based on Items Per
	 * Page and Page Number. Will always return an {@link Integer} > 0.
	 * 
	 * @return The ordinal number of the first item to retrieve based on this
	 *         data
	 */
	public Integer getFirstItem() {
		return (getPageNumber() - 1) * getItemsPerPage();
	}

	/**
	 * Get the desired number of items per page. Will always return a natural
	 * value, that is an integer > 0.
	 * 
	 * @return An {@link Integer} representing the desired number of items per
	 *         page.
	 */
	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	/**
	 * Get the desired page number.
	 * 
	 * @return An {@link Integer} representing the desired page number.
	 */
	public Integer getPageNumber() {
		return pageNumber;
	}

	/**
	 * Set the desired number of items per page. Will only allow values from
	 * natural number range, that is an Integer > 0.
	 * 
	 * @param itemsPerPage
	 *            - the number of items per page
	 * @throws IncorrectPaginationDataException
	 *             when an incorrect value is passed (i.e. the argument is not a
	 *             natural number).
	 */
	public void setItemsPerPage(Integer itemsPerPage) {
		validateNaturalNumber(itemsPerPage, ITEMS_PER_PAGE_FIELD_NAME);
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * Set the desired page number. Will only allow values from natural number
	 * range, that is an Integer > 0.
	 * 
	 * @param pageNumber
	 *            - the page number
	 * @throws IncorrectPaginationDataException
	 *             when an incorrect value is passed (i.e. the argument is not a
	 *             natural number).
	 */
	public void setPageNumber(Integer pageNumber) {
		validateNaturalNumber(pageNumber, PAGE_NUMBER_FIELD_NAME);
		this.pageNumber = pageNumber;
	}

	private void validateNaturalNumber(Integer value, String fieldName) {
		if (value == null || value <= 0) {
			throw new IncorrectPaginationDataException(fieldName, value);
		}
	}

}
