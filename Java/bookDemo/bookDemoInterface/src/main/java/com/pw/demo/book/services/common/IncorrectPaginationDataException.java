package com.pw.demo.book.services.common;

import com.pw.demo.book.exception.BookDemoSystemException;

/**
 * Thrown when an invalid value is passed to one of the setters in
 * {@link PaginationData}
 * 
 * @author Przemyslaw Walat
 */
public class IncorrectPaginationDataException extends BookDemoSystemException {

	private static final long serialVersionUID = -4686971722312962845L;

	/**
	 * Constructor
	 * 
	 * @param field
	 *            - the name of the {@link PaginationData}'s field related with
	 *            the offending value
	 * @param value
	 *            - the offending value
	 */
	public IncorrectPaginationDataException(String field, Integer value) {
		super("Attempted to set invalid value (" + value + ") for " + field);
	}

}
