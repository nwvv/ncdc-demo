Aplikacja Demo z zadaniami z PLSQL i Javy.
Oba rozwiązania potraktowałem jako część większej aplikacji, wydzielając wspólną funkcjonalność tak, aby łatwo można było dopisać kolejne, podobne fragmenty oprogramowania.


AD PLSQL - wykonałem też zadanie opcjonalne, przykładowy plik xml z danymi do importu znajduje się w katalogu /dml. 
Aby wykonać import użytkownik uruchamiający metodę w pakiecie musi posiadać uprawnienia dostępu do katalogu, w którym znajduje się plik xml.
Argumentami są alias do katalogu oraz nazwa pliku.

AD Java - tutaj również wykonałem zadanie opcjonalne, jednakże zabrakło czasu na obsłużenie wyjątków po stronie rest przy pomocy ExceptionMappera. Podczas builda uruchamiane są testy w arquiliianie,
wykonujące deploy na embedded Wildfly'a 8.2.0, także wskazane jest aby nie była w tym momencie uruchomiona żadna inna instancja web servera. Także z braku czasu są tylko pobieżne testy części serwisowej,brakuje testów interfejsów webowych. Podczas deploya EARa aplikacja automatycznie konfiguruje datasource'a pod H2DB. Testowane pod Wildfly 8.2.0.
Jak też wspominałem wcześniej, przy front-endzie w JSF nie ma większej potrzeby mapowania do obiektów DTO, ponieważ obie wartswy są w tym samym JVMie i niewiele zyskujemy, a mapowanie kosztuje; w przypadku RESTów jest to już nieco bardziej uzasadnione, bo nie chcemy oddawać na zwenątrz encji w postaci JSONa, ale tak jak się umawialiśmy,  przygotowałem abstrakcyjną strukturę maperów i wykonuję mapowanie w obu przypadkach aby zademonstrować ich działanie.